const { validate } = require('joi')
const { createSchema, joinSchema } = require('../socketServer/schema'),
  player = require('./subjects/players'),
  { game, waitingGames } = require('./subjects/games'),
  run = require('./run'),
  api = require('../api')

const list = (socket, packet) => {
  let list = waitingGames.map(waitingGame => {
    return {
      uuid: waitingGame.uuid,
      owner: waitingGame.players[0].login,
      rules: waitingGame.rules,
    }
  })
  if (list.length === 0) {
    list = []
  }
  socket.format.send(list)
}

const create = (socket, packet) => {
  const query = packet
  // validation syntax
  const notValid = validate(query, createSchema).error
  if (notValid)
    return socket.format.error(new Error('socket content is not a valid data'))
  // init
  const rules = api.getRuleById(query.rulesId)
  const user = socket.body.user
  const newPlayer = Object.create(player)
  const newGame = Object.create(game)
  // creation
  rules
    .then(res => {
      if (res.data.err) throw err
      newPlayer.init(user.id, user.login, socket)
      newGame.init(newPlayer, res.data.data[0], query.pass)
      waitingGames.push(newGame)
    })
    .catch(function(err) {
      socket.format.error(err)
    })
}

const join = (socket, packet) => {
  const query = packet
  // validation syntax
  if (query) {
    const notValid = validate(query, joinSchema).error
    if (notValid)
      return socket.format.error(
        new Error('socket content is not a valid data')
      )
  }

  // validation semantic
  if (query && query.pass && !query.gameId)
    return socket.format.error(new Error('password sent but no gameId'))

  // init
  const user = socket.body.user
  const newPlayer = Object.create(player)
  newPlayer.init(user.id, user.login, socket)

  // search free
  let index
  if (!query || !query.gameId) {
    index = waitingGames.findIndex(waitingGame => !waitingGame.pass)
    if (index == -1) return socket.format.error(new Error('no free game found'))
  } else {
    //search target
    index = waitingGames.findIndex(
      waitingGame => waitingGame.uuid == query.gameId
    )
    if (index == -1) return socket.format.error(new Error('game not found'))
    if (
      (waitingGames[index].pass && !query.pass) ||
      (waitingGames[index].pass && waitingGames[index].pass != query.pass)
    )
      return socket.format.error(new Error('password error'))
  }
  const recentGame = waitingGames[index]
  const isSameUser =
    recentGame.players.filter(player => {
      return player.userId === socket.body.user.id
    }).length > 0
  if (isSameUser)
    return socket.format.error(new Error('You cannot play against yourself'))
  recentGame.addPlayer(newPlayer)
  waitingGames.splice(index, 1)
  recentGame.players.map(player => {
    player.socket.removeAllListeners('cancelGame')
  })
  run(recentGame)
}

module.exports = {
  create,
  list,
  join,
}
