const { events } = require('events'),
  uuid = require('uuid/v4')
const { format } = require('../../socketServer/output'),
  { io } = require('../../protocol')

const waitingGames = []

const game = {
  init: function(player, rules, pass = false) {
    this.uuid = uuid()
    this.room = io.to(this.uuid)
    this.pass = pass
    this.rules = rules
    this.players = []
    this.round = []
    this.status = ''
    Object.defineProperty(this, 'room', { enumerable: false })
    format(this.room)
    player.socket.on('cancelGame', this.cancel.bind(this))
    this.addPlayer(player)
  },
  changeStatus: function(newStatus) {
    this.status = newStatus
    this.room.format.emit('newStatus', this)
  },
  addPlayer: function(player) {
    this.players.push(player)
    this.players.map(player => {
      player.socket.join(this.uuid)
      player.socket.on('disconnect', this.abort.bind(this))
      Object.defineProperty(player, 'socket', { enumerable: false })
    })
    player.socket.listenMenuTrigger()
    if (this.players.length < 2) this.changeStatus('wait_for_player')
  },
  addRound: function(warriors) {
    this.round.push(warriors)
    this.room.format.emit('roundResolved', this)
  },
  result: function() {
    const winners = this.round.map(warriors =>
      warriors.find(warrior => warrior.winner)
    )
    const resolution = this.players.reduce(
      (winnerId, player, i, array) => {
        const numberWinner = winners.filter(
          winner => winner != undefined && winner.userId == player.userId
        ).length
        return numberWinner > winnerId.number
          ? {
              index: i,
              number: numberWinner,
            }
          : winnerId
      },
      { index: null, number: 0 }
    )
    if (resolution.index != null) this.players[resolution.index].isWinner = true
    this.changeStatus('gameResolved')
  },
  toApi: function(warriorsId) {
    return {
      rules_id: this.rules.id,
      users_ids: this.players.map(player => player.toApi()),
      warriors_ids: warriorsId,
    }
  },
  abort: function() {
    console.log('abort')
    const other = this.players.filter(player => {
      return Object.keys(io.to(this.uuid).connected).find(
        connected => player.socket.id == connected
      )
    })
    if (other) other.isWinner = true
    this.changeStatus('gameAborted')
    this.cancel()
  },
  cancel: function() {
    console.log('cancel')
    const uuid = this.uuid
    let id = waitingGames.findIndex(waitingGame => waitingGame.uuid == uuid)
    if (id != -1) waitingGames.splice(id, 1)

    if (this.status != 'gameAborted') this.changeStatus('delete')
    this.players.map(player => {
      player.socket.listenMenuTrigger()
      player.socket.removeListener('disconnect', () => this.abort)
      player.socket.leave(this.uuid)
    })
  },
  error: function(err) {
    this.room.format.emit('error', null, err)
  },
  emit: function(msg, data) {
    this.room.format.emit(msg, data)
  },
}

module.exports = {
  waitingGames,
  game,
}
