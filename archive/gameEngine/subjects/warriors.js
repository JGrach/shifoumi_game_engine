const warrior = {
  init: function(player, order, weapon = null) {
    this.userId = player.userId
    this.order = order
    this.weapon = weapon
    this.winner = false
  },
  result: function(adversaryWeapon) {
    if (this.weapon.winOn == adversaryWeapon.id) this.winner = true
  },
  toApi: function() {
    return {
      users_id: this.userId,
      order: this.order,
      weapon: this.weapon ? this.weapon.display : null,
      isWinner: this.winner,
    }
  },
}

module.exports = {
  warrior,
}
