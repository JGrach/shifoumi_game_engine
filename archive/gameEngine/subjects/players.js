const player = {
  init: function(userId, login, socket) {
    this.userId = userId
    this.login = login
    this.isWinner = false
    this.socket = socket
  },
  toApi: function() {
    return {
      id: this.userId,
      isWinner: this.isWinner,
    }
  },
}

module.exports = player
