const { validate } = require('joi')
const { warrior } = require('./subjects/warriors'),
  { weapons } = require('./subjects/weapons'),
  { weaponIdSchema } = require('../socketServer/schema'),
  { emit, error } = require('../socketServer/output'),
  { addWarrior, addGame } = require('../api'),
  startDelay = 3000

const save = game => {
  if (game.status != 'gameAborted') game.result()
  const reqWarriors = game.round.reduce((array, warriors) => {
    warriors.map(warrior => {
      array.push(warrior.toApi())
    })
    return array
  }, [])
  Promise.all(reqWarriors.map(req => addWarrior(req)))
    .then(res => {
      return res.map(warriorId => {
        if (warriorId.data.error) throw warriorId.data.error
        return warriorId.data.data.split('id: ')[1]
      })
    })
    .then(warriorsId => {
      return addGame(game.toApi(warriorsId)) //.catch(err => game.error(err))
    })
    .then(gameId => {
      if (gameId.data.error) throw gameId.data.error
      if (game.status != 'gameAborted') game.cancel()
    })
    .catch(err => {
      console.log(err)
      game.error(err)
    })
}

const run = game => {
  const order = game.round.length
  if (order == game.rules.roundNumber || game.status == 'gameAborted')
    return save(game)
  game.changeStatus('ready')

  const warriors = []
  setTimeout(() => {
    if (game.status == 'gameAborted') return save(game)
    game.changeStatus('run')
    game.players.map(player => {
      player.socket.on('choice', weaponId => {
        // validation syntax
        const notValid = validate(weaponId, weaponIdSchema).error
        if (notValid || weaponId >= weapons.length)
          return player.socket.format.error(
            new Error('socket content is not a valid data')
          )
        // validation semantic
        if (
          warriors.findIndex(warrior => warrior.userId == player.userId) != -1
        ) {
          return player.socket.format.error(new Error('You have play already'))
        }
        // init
        const weapon = weapons[weaponId]
        const newWarrior = Object.create(warrior)
        newWarrior.init(player, order, weapon)
        warriors.push(newWarrior)
      })
    })
    game.emit('choose', weapons)
    setTimeout(() => {
      if (game.status == 'gameAborted') return save(game)
      game.players.map(player => player.socket.removeAllListeners('choice'))
      game.emit('roundClose', null)
      // no missing answer:
      if (warriors.length == 2) {
        warriors.map(warrior => {
          const others = warriors.filter(war => warrior.userId != war.userId)
          others.map(other => warrior.result(other.weapon))
        })
        game.addRound(warriors)
        // missing answer:
      } else {
        // answerers are winner
        warriors.map(war => {
          war.winner = true
        })
        // search loosers
        const loosersIdx = game.players.reduce((loosers, player, index) => {
          if (warriors.findIndex(war => war.userId == player.userId) == -1)
            loosers.push(index)
          return loosers
        }, [])
        // create warriors for loosers
        loosersIdx.map(looserIdx => {
          const newWarrior = Object.create(warrior)
          newWarrior.init(game.players[looserIdx], order)
          warriors.push(newWarrior)
        })
        game.addRound(warriors)
      }
      run(game)
    }, game.rules.roundTime)
  }, startDelay)
}

module.exports = run
