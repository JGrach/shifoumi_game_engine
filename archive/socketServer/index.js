const game = require('../gameEngine'),
  { format } = require('./output')

const main = socket => {
  socket.use((packet, next) => {
    socket.listenMenuTrigger = () => {
      let eventsMenu = socket.eventNames()
      eventsMenu = eventsMenu.filter(event => {
        return event == 'list' || event == 'create' || event == 'join'
      })
      if (eventsMenu.length > 0) {
        socket.removeAllListeners('list')
        socket.removeAllListeners('create')
        socket.removeAllListeners('join')
        return
      }
      socket.on('list', packet => game.list(socket, packet))
      socket.on('create', packet => game.create(socket, packet))
      socket.on('join', packet => game.join(socket, packet))
    }
    next()
  })
  socket.on('list', packet => game.list(socket, packet))
  socket.on('create', packet => game.create(socket, packet))
  socket.on('join', packet => game.join(socket, packet))
}

module.exports = main
