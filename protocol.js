const express = require('express'),
  app = express(),
  http = require('http').Server(app)
const io = require('socket.io')(http)

module.exports = { io, http }
