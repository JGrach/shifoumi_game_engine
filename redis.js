const r = require('redis')
const env = process.env.NODE_ENV || 'development',
  { redis } = require('./config.json')[env]
  

client = r.createClient({
  host: redis.host,
  port: redis.port,
})

module.exports = client
