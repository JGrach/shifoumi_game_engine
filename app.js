const { io, http } = require('./protocol'),
  env = process.env.NODE_ENV || 'development',
  {
    game_engine: { port },
  } = require('./config.json')[env],
  { joinWait } = require('./rooms/waitingRoom'),
  { isAuth, test } = require('./socketHandler/middleware'),
  { format } = require('./socketHandler/output')

if(process.env.NODE_ENV == "development") require('dotenv').config('./env')

io.use(format)
io.use(isAuth)
io.on('connection', socket => {
  socket.joinWaitingRoom = () => socket.join('waitingRoom', joinWait(socket))
  socket.joinWaitingRoom()
})

http.listen(port)
