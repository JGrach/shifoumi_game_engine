FROM node:latest

# Create app directory
WORKDIR /usr/src/shifoumi

ADD . .

RUN npm update && npm install


EXPOSE 8888
ENTRYPOINT ["npm", "run"]
CMD [ "start" ]
