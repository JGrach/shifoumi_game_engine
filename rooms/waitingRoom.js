const { onCreateGame, onJoinGame } = require('./events/waitingEvents'),
  { connexionError } = require('../socketHandler/helpers'),
  { waitingList } = require('./entities/gameList'),
  { format } = require('../socketHandler/output'),
  { io } = require('../protocol')

const joinWait = socket => {
  return err => {
    if (err) return connexionError(socket, err)
    socket.format.emit('list', waitingList())
    socket.on('createGame', onCreateGame(socket))
    socket.on('joinGame', onJoinGame(socket))
    socket.leaveWaitingRoom = () =>
      socket.leave('waitingRoom', leaveWait(socket))
  }
}

const leaveWait = socket => {
  return err => {
    if (err) return connexionError(socket, err)
    socket.removeAllListeners('createGame')
    socket.removeAllListeners('joinGame')
    delete socket.leaveWaitingRoom
  }
}

module.exports = {
  joinWait,
  leaveWait,
}
