const { onChoice } = require('./events/gameEvents'),
  { waitingList } = require('./entities/gameList'),
  { round } = require('./entities/round'),
  { format } = require('../socketHandler/output'),
  { connexionError } = require('../socketHandler/helpers'),
  { createGameState } = require('./entities/game'),
  { waitingGames } = require('./entities/game'),
  { weapons } = require('./entities/weapons'),
  { startDelay } = require('./gameConfig'),
  { postWarrior, postGame } = require('../api'),
  { io } = require('../protocol')

const createGame = (socket, rules, pass = false) => {
  return err => {
    const roomId = Object.keys(socket.rooms).find(
      room => room.search(/^game_.+/) != -1
    )
    waitingGames.push(roomId)
    format(io.to(roomId))
    const newGame = io.sockets.adapter.rooms[roomId]
    newGame.state = createGameState(roomId)
    newGame.state.init(rules, pass)
    joinGame(socket)(err)
  }
}

const joinGame = socket => {
  return err => {
    if (err) return connexionError(socket, err)
    const roomId = Object.keys(socket.rooms).find(
      room => room.search(/^game_.+/) != -1
    )
    if (!roomId)
      return connexionError(socket, new Error('Error during creation game'))
    const room = io.sockets.adapter.rooms[roomId]
    const numberPlayer = Object.keys(room.sockets).length
    if (numberPlayer > 2)
      return connexionError(socket, new Error('This room is full'))
    room.state.addPlayer(socket.body.user.id, socket.body.user.login)
    socket.to('waitingRoom').format.emit('list', waitingList()) //broadcast ?
    socket.leaveWaitingRoom()
    socket.leaveGameRoom = () => {
      socket.leave(roomId, leaveGame(socket, roomId))
    }
    socket.on('choice', onChoice(socket))
    socket.on('leaveGame', socket.leaveGameRoom)
    socket.on('disconnect', socket.leaveGameRoom)
    room.state.changeStatus('did_create')
    if (numberPlayer == 2) return runGame(roomId)
  }
}

const runGame = roomId => {
  const room = io.sockets.adapter.rooms[roomId]
  if (
    !room ||
    room.state.status == 'saved' ||
    room.state.status == 'end' ||
    room.state.status == 'deleted'
  )
    return
  const game = room.state
  if (game.order == game.rules.roundNumber) {
    game.result()
    return
  }
  const newRound = Object.create(round)
  newRound.init()
  game.addRound(newRound)
  game.changeStatus('ready')
  setTimeout(() => {
    game.changeStatus('runRound')
    io.in(roomId).format.emit('choose', weapons)
    setTimeout(() => {
      game.changeStatus('resolvingRound')
      for (let i = game.rounds[game.order].round.length - 2; i < 0; i++) {
        const playedId = game.rounds[game.order].round[0]
          ? game.rounds[game.order].round[0].userId
          : null
        const misser = Object.keys(room.sockets).find(
          socketId => io.sockets.sockets[socketId].body.user.id != playedId
        )
        game.rounds[game.order].addWarrior(
          io.sockets.sockets[misser]
            ? io.sockets.sockets[misser].body.user.id
            : null,
          game.order,
          null
        )
      }
      game.rounds[game.order].result()
      game.order++
      game.changeStatus('resolvedRound')
      runGame(roomId)
    }, game.rules.roundTime)
  }, startDelay)
}

const saveGame = game => {
  const reqWarriors = game.rounds.reduce((array, round) => {
    round.toApi().map(warrior => array.push(warrior))
    return array
  }, [])
  Promise.all(reqWarriors.map(warrior => postWarrior(warrior)))
    .then(res => res.map(warriorId => warriorId.data.data.split('id: ')[1]))
    .then(warriorsId => {
      postGame(game.outputApi(warriorsId)).catch(err => {
        console.log(err)
      })
    })
    .catch(err => {
      console.log(err)
    })
}

const leaveGame = (socket, roomId) => {
  return err => {
    if (err) return connexionError(socket, err)
    socket.removeAllListeners('choice')
    socket.removeAllListeners('leaveGame')
    socket.removeListener('disconnect', leaveGame(socket))
    delete socket.leaveGameRoom
    const id = waitingGames.findIndex(waitingGame => waitingGame == roomId)
    if (id != -1)
      waitingGames.splice(id, 1) &&
        socket.to('waitingRoom').format.emit('list', waitingList())
    const room = io.sockets.adapter.rooms[roomId]
    socket.joinWaitingRoom()
    if (!room) return

    if (room.state.status != 'end' && room.state.status != 'saved') {
      room.state.result()
    }

    if (room.state.status != 'saved') {
      saveGame(room.state)
      room.state.changeStatus('saved')
    }

    if (Object.keys(io.sockets.adapter.rooms[roomId].sockets).length > 0) {
      Object.keys(io.sockets.adapter.rooms[roomId].sockets).map(socketId => {
        io.sockets.sockets[socketId].leave(
          roomId,
          leaveGame(io.sockets.sockets[socketId])
        )
      })
    }
  }
}

module.exports = {
  joinGame,
  leaveGame,
  createGame,
}
