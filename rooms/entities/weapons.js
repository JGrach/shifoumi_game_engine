const weapons = [
  {
    id: 0,
    display: 'rock',
    winOn: 2,
  },
  {
    id: 1,
    display: 'paper',
    winOn: 0,
  },
  {
    id: 2,
    display: 'scissors',
    winOn: 1,
  },
]

module.exports = {
  weapons,
}
