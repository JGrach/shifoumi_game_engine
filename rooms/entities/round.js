const round = {
  init: function() {
    this.round = []
  },
  addWarrior: function(userId, order, weapon) {
    if (!userId) return
    const warrior = { userId, order, weapon, winner: false }
    this.round.push(warrior)
    // if (this.round.length == 2) this.result()
  },
  result: function() {
    this.round.map(warrior => {
      const adversary = this.round.find(war => warrior.userId != war.userId)
      if (!warrior.weapon) return
      if (!adversary.weapon || warrior.weapon.winOn == adversary.weapon.id)
        warrior.winner = true
    })
  },
  toApi: function() {
    return this.round.map(warrior => {
      return {
        users_id: warrior.userId,
        order: warrior.order,
        weapon: warrior.weapon ? warrior.weapon.display : null,
        isWinner: warrior.winner,
      }
    })
  },
}

module.exports = {
  round,
}
