const { io } = require('../../protocol')

const waitingGames = []

const createGameState = roomId => {
  return Object.create({
    init: function(rules, pass) {
      this.status = ''
      this.order = 0
      this.rules = rules
      this.rounds = []
      this.users = []
      this.pass = pass
    },
    addPlayer: function(userId, login) {
      this.users.push({
        userId,
        login,
        isWinner: false,
      })
    },
    changeStatus: function(newStatus) {
      if (
        (this.status == 'end' && newStatus != 'saved') ||
        (this.status == 'saved' && newStatus != 'deleted') ||
        this.status == 'deleted' ||
        this.status == newStatus
      )
        return
      this.status = newStatus
      io.in(roomId).format.emit('changeStatus', this)
    },
    addRound: function(round) {
      if (
        this.status == 'saved' ||
        this.status == 'end' ||
        this.status == 'deleted'
      )
        return
      this.rounds.push(round)
      this.changeStatus('round_added')
    },
    result: function() {
      if (Object.keys(io.sockets.adapter.rooms[roomId].sockets).length < 2) {
        const winnerSocket = Object.keys(
          io.sockets.adapter.rooms[roomId].sockets
        )[0]
        const winner = io.sockets.sockets[winnerSocket].body.user.id
        this.users.map(player => {
          if (player.userId == winner) player.isWinner = true
        })
      } else {
        this.users.map(player => {
          if (!player.numberWin) {
            player.numberWin = 0
            Object.defineProperty(player, 'numberWin', { enumerable: false })
          }
          this.rounds.map(round =>
            round.round.map(warrior => {
              if (warrior.userId == player.userId && warrior.winner)
                player.numberWin++
            })
          )
        })
        this.users.map(player => {
          this.users.map(other => {
            if (
              player.userId != other.userId &&
              player.numberWin > other.numberWin
            ) {
              player.isWinner = true
            }
          })
        })
      }
      this.changeStatus('end')
    },
    outputApi: function(databaseWarriorsId) {
      return {
        rules_id: this.rules.id,
        users_objects: this.users.map(player => ({
          id: player.userId,
          isWinner: player.isWinner,
        })),
        warriors_ids: databaseWarriorsId,
      }
    },
  })
}

module.exports = {
  waitingGames,
  createGameState,
}
