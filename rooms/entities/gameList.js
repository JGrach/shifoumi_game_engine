const { io } = require('../../protocol'),
  { waitingGames } = require('./game')

const gameList = uuidArray => {
  return uuidArray.reduce((acc, uuid, i) => {
    if (
      !io.sockets.adapter.rooms[uuid] ||
      !io.sockets.adapter.rooms[uuid].state
    ) {
      uuidArray.splice(i, 1)
      return acc
    }
    acc.push({ gameId: uuid, ...io.sockets.adapter.rooms[uuid].state })
    return acc
  }, [])
}

const waitingList = () => gameList(waitingGames)

module.exports = {
  gameList,
  waitingList,
}
