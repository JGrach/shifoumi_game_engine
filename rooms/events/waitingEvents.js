const { validate } = require('joi'),
  uuid = require('uuid/v4')
const { io } = require('../../protocol'),
  { createSchema, joinSchema } = require('../../socketHandler/schema'),
  api = require('../../api'),
  { createGame, joinGame } = require('../gameRoom'),
  { waitingGames } = require('../entities/game')

const onCreateGame = socket => {
  return packet => {
    const query = packet
    // validation syntax
    const notValid = validate(query, createSchema).error
    if (notValid)
      return socket.format.error(
        new Error('socket content is not a valid data')
      )
    // validation semantic
    // only 1 game
    const isGaming = Object.keys(socket.rooms).find(room => {
      return room.search(/^game_.+/) != -1
    })
    if (isGaming) return socket.format.error(new Error('you already gaming'))

    // init
    const rules = api.getRuleById(query.rulesId)
    rules
      .then(res => {
        if (res.data.err) throw err
        if (res.data.data.length != 1)
          throw new Error('Error during loading rules')
        const roomId = 'game_' + uuid()
        socket.join(roomId, createGame(socket, res.data.data[0], query.pass))
      })
      .catch(function(err) {
        //console.log(err)
        socket.format.error(err)
      })
  }
}

const onJoinGame = socket => {
  return packet => {
    const query = packet
    // validation syntax
    if (query) {
      const notValid = validate(query, joinSchema).error
      if (notValid)
        return socket.format.error(
          new Error('socket content is not a valid data')
        )
    }

    // validation semantic
    // a password but no target
    if (query && query.pass && !query.gameId)
      return socket.format.error(new Error('password sent but no gameId'))

    // only 1 game
    const isGaming = Object.keys(socket.rooms).find(room => {
      return room.search(/^game_.+/) != -1
    })
    if (isGaming) return socket.format.error(new Error('you already gaming'))

    // search free
    let index
    if (!query || !query.gameId) {
      index = waitingGames.findIndex(
        waitingGame => !io.sockets.adapter.rooms[waitingGame].state.pass
      )
      if (index == -1)
        return socket.format.error(new Error('no free game found'))
    } else {
      //search target
      index = waitingGames.findIndex(waitingGame => waitingGame == query.gameId)
      if (index == -1) return socket.format.error(new Error('game not found'))
      if (
        (io.sockets.adapter.rooms[waitingGames[index]].state.pass &&
          !query.pass) ||
        (io.sockets.adapter.rooms[waitingGames[index]].state.pass &&
          io.sockets.adapter.rooms[waitingGames[index]].state.pass !=
            query.pass)
      )
        return socket.format.error(new Error('password error'))
    }
    socket.join(waitingGames[index], joinGame(socket))
    waitingGames.splice(index, 1)
  }
}

module.exports = {
  onCreateGame,
  onJoinGame,
}
