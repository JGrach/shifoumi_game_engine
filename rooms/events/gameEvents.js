const { validate } = require('joi')
const { io } = require('../../protocol'),
  { weaponIdSchema } = require('../../socketHandler/schema'),
  { weapons } = require('../entities/weapons')

const onChoice = socket => {
  return packet => {
    const query = packet
    const roomId = Object.keys(socket.rooms).find(
      room => room.search(/^game_.+/) != -1
    )
    const room = io.sockets.adapter.rooms[roomId]
    if (room.state.status != 'runRound') return
    const notValid = validate(query, weaponIdSchema).error
    if (notValid)
      return socket.format.error(
        new Error('socket content is not a valid data')
      )
    if (
      room.state.rounds[room.state.order].round.findIndex(
        warrior => warrior.userId == socket.body.user.id
      ) != -1
    ) {
      return socket.format.error(new Error('You have play already'))
    }
    room.state.rounds[room.state.order].addWarrior(
      socket.body.user.id,
      room.state.order,
      weapons.find(weapon => weapon.id == query)
    )
  }
}

module.exports = {
  onChoice,
}
