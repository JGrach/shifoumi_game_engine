const jwt = require('jsonwebtoken'),
  { waterfall, every } = require('async')
const redis = require('../redis')

function updateAuth(redisId, update, cb) {
  every(
    Object.keys(update).map(key => {
      const pair = {}
      pair[key] = update[key]
      return pair
    }),
    (pair, callback) => {
      const key = Object.keys(pair)
      if (key.length > 1)
        return callback(
          new Error(
            'Error during user object decomposition for Redis newAuth()'
          )
        )
      redis.HSET(redisId, key[0], update[key[0]], callback)
    },
    (err, res) => {
      if (err) return cb(err)
      cb(null, redisId)
    }
  )
}

const isAuth = (socket, next) => {
  const token =
    socket.handshake && socket.handshake.query && socket.handshake.query.token
  if (!token) return next(new Error('You have to be connected !'))
  waterfall(
    [
      callback => {
        jwt.verify(token, process.env.JWT_SECRET, callback)
      },
      (redisId, callback) => {
        redis.HGETALL(redisId, (err, res) => {
          if (err) return callback(err)
          if (!res)
            return callback(
              new Error('Token invalid or expired, please reconnect')
            )
          callback(null, redisId, res)
        })
      },
      (redisId, user, callback) => {
        updateAuth(redisId, { timestamp: Date.now() }, (err, res) => {
          if (err) return callback(err)
          callback(null, user)
        })
      },
    ],
    (err, user) => {
      if (err) return socket.format.error(err)
      user.isAdmin = user.isAdmin == 'true'
      socket.body = { user }
      next()
    }
  )
}

module.exports = {
  isAuth,
}
