const connexionError = (socket, err) => {
  socket.format.error(err) && socket.disconnect('true')
}

module.exports = {
  connexionError,
}
