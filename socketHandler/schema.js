const Joi = require('joi')

const createSchema = Joi.object().keys({
  rulesId: Joi.number()
    .integer()
    .required(),
  pass: Joi.string()
    .max(30)
    .optional(),
})

const joinSchema = Joi.object().keys({
  gameId: Joi.string()
    .max(50)
    .optional(),
  pass: Joi.string()
    .max(30)
    .optional(),
})

const weaponIdSchema = Joi.number()
  .integer()
  .min(0)
  .max(3)

module.exports = {
  createSchema,
  joinSchema,
  weaponIdSchema,
}
