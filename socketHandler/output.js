function emit(msg, data, error = null) {
  this.emit(msg, {
    err: error ? JSON.stringify(error) : null,
    data: data ? JSON.stringify(data) : null,
  })
}

function send(data) {
  this.send({
    err: null,
    data: JSON.stringify(data),
  })
}

function error(err) {
  this.error({
    err: JSON.stringify(err.message ? err.message : err), // TODO: logger
    data: null,
  })
}

const format = (socket, next) => {
  socket.format = {}
  if (typeof socket.send !== 'undefined') socket.format.send = send.bind(socket)
  if (typeof socket.emit !== 'undefined') socket.format.emit = emit.bind(socket)
  if (typeof socket.error !== 'undefined')
    socket.format.error = error.bind(socket)
  if (next) next()
}

module.exports = {
  send,
  error,
  emit,
  format,
}
