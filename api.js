const axios = require('axios')
const env = process.env.NODE_ENV || 'development',
  { api } = require('./config.json')[env],
  baseURL = 'http://' + api.host + ':' + api.port + '/v' + api.version

// TODO Retry connexion if error

const isExpiration = err => {
  if (
    err.response &&
    err.response.status == 401 &&
    err.response.data.err == '401: Token invalid or expired, please reconnect'
  ) {
    return true
  }
  return false
}

const getToken = () => {
  return axios
    .post(baseURL + '/login', {
      login: process.env.USERNAME,
      password: process.env.PASSWORD,
    })
    .then(res => {
      return axios.create({
        baseURL,
        timeout: 1000,
        headers: { Authorization: 'bearer ' + res.data.data.token },
      })
    })
    .catch(err => {
      console.log(err) // TODO err
      process.exit(1)
    })
}

let token = getToken()

const refreshToken = (recall, params) => {
  token = getToken()
  return recall(...params)
}

const getRuleById = (id, retry = true) => {
  return token.then(request => request.get('/rule?id=' + id)).catch(err => {
    if (isExpiration(err) && retry)
      return refreshToken(getRuleById, [id, false])
    return err
  })
}

const postWarrior = (warrior, retry = true) => {
  return token
    .then(request => request.post('/admin/warrior', warrior))
    .catch(err => {
      if (isExpiration(err) && retry)
        return refreshToken(addWarrior, [warrior, false])
      return err
    })
}

const postGame = (game, retry = true) => {
  return token.then(request => request.post('/admin/game', game)).catch(err => {
    if (isExpiration(err) && retry)
      return refreshToken(addGame, [warrior, true])
    return err
  })
}

module.exports = {
  getRuleById,
  postWarrior,
  postGame,
}
